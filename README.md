# CERN CI Web Deployer

Docker image to be used with GitLab CI to deploy web sites or generic files or binaries generated from GitLab repositories to any of the CERN Web services platform including EOS.

## Security considerations

Credentials (username and password) of a CERN account, that is used for deploying website content, will be stored in GitLab variables in clear-text. To limit security impact in case of credential exposure, please use a dedicated secondary or service account, with very limited privileges (write access to that particular web site - but ideally no other privileges). **Do not use your primary CERN account, nor any privileged service account.**

## Version

1.8
  - Use SMB3 protocol for DFS by default, but allow users to configure it through the `SMB_PROTOCOL` variable.

1.7
  - Support deployment to EOS personal/project/workspace folder

1.6
  - Moved docker image to the GitLab registry

1.5
  - Support for [CERN DFS websites](https://espace.cern.ch/webservices-help/websitemanagement/ConfiguringCentrallyHostedSites-IIS7/Pages/default.aspx)

## Contents
* *deploy-dfs*: Publish in a DFS folder the contents provided. Makes use of environment variables, *Kerberos* for authentication and *smbclient* to interact with DFS.
  * `DFS_WEBSITE_USERNAME`: Name of the account to be used for the deployment (see [Security considerations](#security-considerations) above)
  * `DFS_WEBSITE_PASSWORD`: Accounts's password
  * `CI_WEBSITE_DIR` (optional): Local folder to where files/folders to be deployed are located. **Default:** `public/`
  * `DFS_WEBSITE_NAME`: Name of the DFS website (Example: "test-gitlab-pages" whose URL would be https://test-gitlab-pages.web.cern.ch)
  * `DFS_WEBSITE_PATH`: Path in DFS to copy the files to. If this is used, `DFS_WEBSITE_NAME` is ignored.
  * `DFS_WEBSITE_DIR` (optional): A subfolder of the DFS web site where to deploy pages (e.g. `mydocs` to publish to https://test-gitlab-pages.web.cern.ch/mydocs). **Default** publish to web site root folder.
  * `SMB_PROTOCOL` (optional): Set the SAMBA protocol to be used for the transfer. By default, it is set to `smb3`

* *deploy-eos*: Publish in a EOS folder the contents provided. Makes use of environment variables, *Kerberos* for authentication and *lxplus.cern.ch* as bridge to access to EOS.
  * `EOS_ACCOUNT_USERNAME`: Name of the account to be used for the deployment (see [Security considerations](#security-considerations) above). Must have RW access to the EOS folder
  * `EOS_ACCOUNT_PASSWORD`: Accounts's password
  * `CI_OUTPUT_DIR` (optional): Local folder to be rsynced with EOS folder. **Default:** `public/`
  * `EOS_PATH`: EOS path where to deploy the contents generated in `CI_OUTPUT_DIR`
  * `EOS_MGM_URL` (optional): The MGM URL of the EOS instance. **Default:** `root://eosuser.cern.ch`
  * `METHOD` (optional): Method to do the synchronization. It can be `rsync` or `xrdcp`. The rsync method relies on connecting to lxplus, and will make sure that the files deleted in source are also deleted on destination. The account (`EOS_ACCOUNT_USERNAME`) used must be able to log in lxplus. On the other hand xrdcp will directly connect to EOS, but will not delete files on the destination. **Default**: `xrdcp`

## Gitlab-ci exmaple

This is a simple example of deploying a web site to EOS. One must simply put the following extract into ```.gitlab-ci.yaml```:

```
deploy:
  stage: deploy
  variables:
    "EOS_PATH": "/eos/user/e/example"
  image: gitlab-registry.cern.ch/ci-tools/ci-web-deployer
  script:
    - deploy-eos
```

This will copy every file in the sub-directory ```public``` into the folder ```/eos/user/e/example```. The variables ```EOS_ACCOUNT_USERNAME``` and  ```EOS_ACCOUNT_PASSWORD``` must be defined in ```Settings > CI /CD > Variables```.  See the list of variables above for more information.

## Download
```sh
docker pull gitlab-registry.cern.ch/ci-tools/ci-web-deployer
```
